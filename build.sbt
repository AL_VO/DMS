name := "DMS"

version := "1.0"

scalaVersion := "2.10.4"

libraryDependencies += "org.apache.commons" % "commons-email" % "1.2"

libraryDependencies += "io.spray" %%  "spray-json" % "1.3.1"